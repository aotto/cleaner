<?php
/***************************************************************
 *  Copyright notice
 *  (c) 2011 Andreas Otto <andreas@otto-hanika.de>
 *  All rights reserved
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Class "tx_cleaner_Delete" provides db procedures to mark entries
 * in selected db tables as deleted.
 *
 * @author		Andreas Otto <andreas@otto-hanika.de>
 * @package		TYPO3
 * @subpackage	tx_cleaner
 * $Id$
 */
class tx_cleaner_Delete extends tx_scheduler_Task {

	/**
	 * An email address to be used during the process
	 *
	 * @var	string		$email
	 */
	var $email;

	/**
	 * A table name whose data should be marked as deleted
	 *
	 * @var string	  $table
	 */
	var $table;

	/**
	 * A page ID which can be used to limit the task in addition to the age of the records
	 *
	 * @var int			$pid
	 */
	var $pageId;

	/**
	 * The age in days of records which will marked as deleted
	 *
	 * @var int		 $age
	 */
	var $age;

	/**
	 * The age in seconds of records which will marked as deleted, based on var $age
	 *
	 * @var int			$ageInSeconds
	 */
	var $ageInSeconds;

	/**
	 * The age in days of records which will marked as deleted formatted as string
	 *
	 * @var string		$ageInDaysAsString
	 */
	var $ageInDaysAsString;

	/**
	 * @var string		$sqlWhereClause
	 */
	var $sqlWhereClause;

	/**
	 * @var array		$toBeDeleted
	 */
	var $toBeDeleted;

	/**
	 * Function executed from the Scheduler.
	 *
	 * @return	bool
	 */
	public function execute() {
		if (empty($this->ageInSeconds)) {
			$this->setAgeInSeconds();
		}

		if (empty($this->ageInDaysAsString)) {
			$this->setAgeInDaysAsString();
		}

		if (empty($this->sqlWhereClause)) {
			$this->setSqlWhereClause();
		}

		$this->toBeDeleted = $this->findToBeDeleted();

		$success = $this->Delete();

		if ($success) {
			$success = $this->sendEmailNotification();
		}

		t3lib_div::sysLog('[tx_cleaner_Delete]: "' . count($this->toBeDeleted) . '" records older than "' . $this->getAgeInDaysAsString() . '" from table "' . $this->table . '" were deleted.', 'cleaner', 0);

		return $success;
	}

	/**
	 * This method returns the destination mail address as additional information
	 *
	 * @return	string	Information to display
	 */
	public function getAdditionalInformation() {
		return $GLOBALS['LANG']->sL('LLL:EXT:cleaner/locallang.xml:label.table') . ': ' . $this->table . ', ' . $this->age;
	}

	/**
	 * Mark records as deleted.
	 *
	 * @return bool
	 */
	protected function Delete() {
		$success = $GLOBALS['TYPO3_DB']->exec_DELETEquery(
			$this->table,
			$this->getSqlWhereClause()
		);

		t3lib_div::sysLog('[tx_cleaner_Delete]: Deleting from table "' . $this->table . '" using "' . $this->getSqlWhereClause() . '".', 'cleaner', 0);
		return $success;
	}

	/**
	 * Find records which will be deleted
	 *
	 * @return array
	 */
	protected function findToBeDeleted() {
		return $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('uid,pid,' . $GLOBALS['TCA'][$this->table]['ctrl']['crdate'] . ',' . $GLOBALS['TCA'][$this->table]['ctrl']['label'], $this->table, $this->getSqlWhereClause());
	}

	/**
	 * Sends a notification email.
	 *
	 * @throws t3lib_exception
	 * @return bool
	 */
	protected function sendEmailNotification() {
		$success = FALSE;
		if (!empty($this->email)) {
			// Get execution information
			$exec = $this->getExecution();

			// Get call method
			if (basename(PATH_thisScript) == 'cli_dispatch.phpsh') {
				$calledBy = 'CLI module dispatcher';
				$site = '-';
			} else {
				$calledBy = 'TYPO3 backend';
				$site = t3lib_div::getIndpEnv('TYPO3_SITE_URL');
			}

			$start = $exec->getStart();
			$end = $exec->getEnd();
			$interval = $exec->getInterval();
			$multiple = $exec->getMultiple();
			$cronCmd = $exec->getCronCmd();
			$mailBody =
					'Cleaner, delete task' . LF
							. '- - - - - - - - - - - - - - - -' . LF
							. 'UID: ' . $this->taskUid . LF
							. 'Sitename: ' . $GLOBALS['TYPO3_CONF_VARS']['SYS']['sitename'] . LF
							. 'Site: ' . $site . LF
							. 'Called by: ' . $calledBy . LF
							. 'Date: ' . date('Y-m-d H:i:s') . ' [' . time() . ']' . LF
							. LF
							. 'Table: ' . $this->table . LF
							. 'Age: ' . $this->getAgeInDaysAsString() . LF
							. 'Records: ' . $records = count($this->toBeDeleted) . LF;

			if ($records > 0) {
				$mailBody .= LF
						. 'Uid;Pid;Crdate;Name' . LF;
				foreach ($this->toBeDeleted as $row) {
					$mailBody .= $row['uid'] . ';' . $row['pid'] . ';' . date('Y-m-d H:i:s', $row[$GLOBALS['TCA'][$this->table]['ctrl']['crdate']]) . ';' . $row[$GLOBALS['TCA'][$this->table]['ctrl']['label']] . LF;
				}
			}

			// Prepare mailer and send the mail
			try {
				/** @var $mailer t3lib_mail_message */
				$mailer = t3lib_div::makeInstance('t3lib_mail_message');
				$mailer->setFrom(array($this->email => 'Cleaner'));
				$mailer->setReplyTo(array($this->email => 'Cleaner'));
				$mailer->setSubject('Cleaner, delete task, ' . $this->table);
				$mailer->setBody($mailBody);
				$mailer->setTo($this->email);
				$mailsSend = $mailer->send();
				$success = ($mailsSend > 0);
				t3lib_div::sysLog('[tx_cleaner_Delete]: Email report sent to "' . $this->email . '"', 'cleaner', 0);
			} catch (Exception $e) {
				throw new t3lib_exception($e->getMessage());
			}
		} else {
			// No email defined, just log the task
			t3lib_div::sysLog('[tx_cleaner_Delete]: No email address given', 'cleaner', 2);
			$success = TRUE;
		}

		return $success;
	}

	/**
	 * Set age in seconds
	 *
	 * @return void
	 */
	public function setAgeInSeconds() {
		$this->ageInSeconds = 86400 * $this->age;
	}

	/**
	 * Get age in seconds
	 *
	 * @return int
	 */
	public function getAgeInSeconds() {
		return $this->ageInSeconds;
	}

	/**
	 * Set age in days formatted as string
	 *
	 * @return void
	 */
	public function setAgeInDaysAsString() {
		$this->ageInDaysAsString = ($this->age > 1) ? $this->age . ' ' . $GLOBALS['LANG']->sL('LLL:EXT:cleaner/locallang.xml:label.days') : $this->age . ' ' . $GLOBALS['LANG']->sL('LLL:EXT:cleaner/locallang.xml:label.day');
	}

	/**
	 * Get age in days formatted as string
	 *
	 * @return string
	 */
	public function getAgeInDaysAsString() {
		return $this->ageInDaysAsString;
	}

	/**
	 * Set sqlWhereClause
	 *
	 * @return void
	 */
	public function setSqlWhereClause() {
		$where = $GLOBALS['TCA'][$this->table]['ctrl']['delete'] . ' = 1 AND ' . $GLOBALS['TCA'][$this->table]['ctrl']['crdate'] . ' + ' . $this->getAgeInSeconds() . ' < ' . time();
		if (!empty($this->pageId)) {
			$where .= ' AND pid = ' . $this->pageId;
		}
		$this->sqlWhereClause = $where;
	}

	/**
	 * Get SQL where-clause
	 *
	 * @return string
	 */
	public function getSqlWhereClause() {
		return $this->sqlWhereClause;
	}
}

if (defined('TYPO3_MODE') && isset($GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/cleaner/taks/class.tx_cleaner_delete.php'])) {
	/** @noinspection PhpIncludeInspection */
	include_once($GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/cleaner/tasks/class.tx_cleaner_delete.php']);
}

?>