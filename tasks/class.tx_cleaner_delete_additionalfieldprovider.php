<?php
/***************************************************************
 *  Copyright notice
 *  (c) 2011 Andreas Otto <andreas@otto-hanika.de>
 *  All rights reserved
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Aditional fields provider class for usage with the Cleaner's delete task
 *
 * @author		Andreas Otto <andreas@otto-hanika.de>
 * @package		TYPO3
 * @subpackage	tx_cleaner
 * $Id$
 */
class tx_cleaner_Delete_AdditionalFieldProvider implements tx_scheduler_AdditionalFieldProvider {

	/**
	 * This method is used to define new fields for adding or editing a task
	 * In this case, it adds an email field
	 *
	 * @param	array					$taskInfo: reference to the array containing the info used in the add/edit form
	 * @param	object					$task: when editing, reference to the current task object. Null when adding.
	 * @param	tx_scheduler_Module		$parentObject: reference to the calling object (Scheduler's BE module)
	 * @return	array					Array containg all the information pertaining to the additional fields
	 *									The array is multidimensional, keyed to the task class name and each field's id
	 *									For each field it provides an associative sub-array with the following:
	 *										['code']		=> The HTML code for the field
	 *										['label']		=> The label of the field (possibly localized)
	 *										['cshKey']		=> The CSH key for the field
	 *										['cshLabel']	=> The code of the CSH label
	 */
	public function getAdditionalFields(array &$taskInfo, $task, tx_scheduler_Module $parentObject) {
		$additionalFields = array();

		// Initialize extra field value
		if (empty($taskInfo['tx_cleaner_Delete_email'])) {
			if ($parentObject->CMD == 'add') {
				// In case of new task and if field is empty, set default email address
				$taskInfo['tx_cleaner_Delete_email'] = $GLOBALS['BE_USER']->user['email'];
			} elseif ($parentObject->CMD == 'edit') {
				// In case of edit, and editing a test task, set to internal value if not data was submitted already
				$taskInfo['tx_cleaner_Delete_email'] = $task->email;
			} else {
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['tx_cleaner_Delete_email'] = '';
			}
		}
		// Write the code for the field email
		$fieldID = 'task_tx_cleaner_Delete_email';
		$fieldCode = '<input type="text" name="tx_scheduler[tx_cleaner_Delete_email]" id="' . $fieldID . '" value="' . htmlspecialchars($taskInfo['tx_cleaner_Delete_email']) . '" size="30" />';
		$additionalFields[$fieldID] = array(
			'code' => $fieldCode,
			'label' => 'LLL:EXT:cleaner/locallang.xml:label.email',
			'cshKey' => 'cleaner',
			'cshLabel' => $fieldID
		);

		// Initialize extra field value
		if (empty($taskInfo['tx_cleaner_Delete_table'])) {
			if ($parentObject->CMD == 'add') {
				// In case of new task and if field is empty, set default table
				$taskInfo['table'] = '';
			} elseif ($parentObject->CMD == 'edit') {
				// In case of edit, and editing a task, set to internal value if not data was submitted already
				$taskInfo['tx_cleaner_Delete_table'] = $task->table;
			} else {
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['tx_cleaner_Delete_table'] = '';
			}
		}
		// Write the code for the field table
		$fieldID = 'task_tx_cleaner_Delete_table';
		$tables = array_keys($GLOBALS['TCA']);
		$selectValues = '<option></option>';
		foreach ($tables as $table) {
			if (strpos($table, 'tx_') !== FALSE) {
				if (htmlspecialchars($task->table) == $table) {
					$selected = ' selected="selected"';
				} else {
					$selected = '';
				}
				$selectValues .= sprintf('<option value="%1$s"%2$s>%1$s</option>', $table, $selected);
			}
		}
		$fieldCode = sprintf('<select name="tx_scheduler[tx_cleaner_Delete_table]" id="%s" size="1">%s</select>', $fieldID, $selectValues);
		$additionalFields[$fieldID] = array(
			'code' => $fieldCode,
			'label' => 'LLL:EXT:cleaner/locallang.xml:label.table',
			'cshKey' => 'cleaner',
			'cshLabel' => $fieldID
		);

		// Initialize extra field value
		if (empty($taskInfo['tx_cleaner_Delete_pageId'])) {
			if ($parentObject->CMD == 'add') {
				// In case of new task and if field is empty, keep it this way
				$taskInfo['tx_cleaner_Delete_pageId'] = '';
			} elseif ($parentObject->CMD == 'edit') {
				// In case of edit, and editing a test task, set to internal value if not data was submitted already
				$taskInfo['tx_cleaner_Delete_pageId'] = $task->pageId;
			} else {
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['tx_cleaner_Delete_pageId'] = '';
			}
		}
		// Write the code for the field pageId
		$fieldID = 'task_tx_cleaner_Delete_pageid';
		$fieldCode = '<input type="text" name="tx_scheduler[tx_cleaner_Delete_pageId]" id="' . $fieldID . '" value="' . htmlspecialchars($taskInfo['tx_cleaner_Delete_pageId']) . '" size="30" />';
		$additionalFields[$fieldID] = array(
			'code' => $fieldCode,
			'label' => 'LLL:EXT:cleaner/locallang.xml:label.pageId',
			'cshKey' => 'cleaner',
			'cshLabel' => $fieldID
		);

		// Initialize extra field value
		if (empty($taskInfo['tx_cleaner_Delete_age'])) {
			if ($parentObject->CMD == 'add') {
				// In case of new task and if field is empty, set default age
				$taskInfo['tx_cleaner_Delete_age'] = '365';
			} elseif ($parentObject->CMD == 'edit') {
				// In case of edit, and editing a task, set to internal value if not data was submitted already
				$taskInfo['tx_cleaner_Delete_age'] = $task->age;
			} else {
				// Otherwise set an empty value, as it will not be used anyway
				$taskInfo['tx_cleaner_Delete_age'] = '';
			}
		}
		// Write the code for the field age
		$fieldID = 'task_tx_cleaner_Delete_age';
		$values = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 30, 60, 90, 180, 270, 365);
		$selectValues = '<option></option>';
		foreach ($values as $value) {
			if (htmlspecialchars($task->age) == $value) {
				$selected = ' selected="selected"';
			} else {
				$selected = '';
			}
			$selectValues .= sprintf('<option value="%1$d"%3$s>%1$d %2$s</option>', $value, ($value > 1) ? $GLOBALS['LANG']->sL('LLL:EXT:cleaner/locallang.xml:label.days') : $GLOBALS['LANG']->sL('LLL:EXT:cleaner/locallang.xml:label.day'), $selected);
		}
		$fieldCode = sprintf('<select name="tx_scheduler[tx_cleaner_Delete_age]" id="%s" size="1">%s</select>', $fieldID, $selectValues);
		$additionalFields[$fieldID] = array(
			'code' => $fieldCode,
			'label' => 'LLL:EXT:cleaner/locallang.xml:label.age',
			'cshKey' => 'cleaner',
			'cshLabel' => $fieldID
		);

		return $additionalFields;
	}

	/**
	 * This method checks any additional data that is relevant to the specific task
	 * If the task class is not relevant, the method is expected to return true
	 *
	 * @param	array					$submittedData: reference to the array containing the data submitted by the user
	 * @param	tx_scheduler_Module		$parentObject: reference to the calling object (Scheduler's BE module)
	 * @return	boolean					True if validation was ok (or selected class is not relevant), false otherwise
	 */
	public function validateAdditionalFields(array &$submittedData, tx_scheduler_Module $parentObject) {
		$result = TRUE;
		$submittedData['tx_cleaner_Delete_email'] = strip_tags($submittedData['tx_cleaner_Delete_email']);
		$submittedData['tx_cleaner_Delete_table'] = strip_tags($submittedData['tx_cleaner_Delete_table']);
		$submittedData['tx_cleaner_Delete_pageId'] = trim($submittedData['tx_cleaner_Delete_pageId']);
		$submittedData['tx_cleaner_Delete_age'] = intval($submittedData['tx_cleaner_Delete_age']);

		if (!empty($submittedData['tx_cleaner_Delete_email']) && !t3lib_div::validEmail($submittedData['tx_cleaner_Delete_email'])) {
			$parentObject->addMessage($GLOBALS['LANG']->sL('LLL:EXT:cleaner/locallang.xml:msg.noEmail'), t3lib_FlashMessage::ERROR);
			$result = FALSE;
		}

		if (empty($submittedData['tx_cleaner_Delete_table'])) {
			$parentObject->addMessage($GLOBALS['LANG']->sL('LLL:EXT:cleaner/locallang.xml:msg.noTable'), t3lib_FlashMessage::ERROR);
			$result = FALSE;
		}

		if ((!empty($submittedData['tx_cleaner_Delete_pageId']) && !intval($submittedData['tx_cleaner_Delete_pageId'])) || (!empty($submittedData['tx_cleaner_Delete_pageId']) && $submittedData['tx_cleaner_Delete_pageId'] < 0)) {
			$parentObject->addMessage($GLOBALS['LANG']->sL('LLL:EXT:cleaner/locallang.xml:msg.noPid'), t3lib_FlashMessage::ERROR);
			$result = FALSE;
		}

		if (empty($submittedData['tx_cleaner_Delete_age'])) {
			$parentObject->addMessage($GLOBALS['LANG']->sL('LLL:EXT:cleaner/locallang.xml:msg.noAge'), t3lib_FlashMessage::ERROR);
			$result = FALSE;
		}

		return $result;
	}

	/**
	 * This method is used to save any additional input into the current task object
	 * if the task class matches
	 *
	 * @param	array				$submittedData: array containing the data submitted by the user
	 * @param	tx_scheduler_Task	$task: reference to the current task object
	 * @return	void
	 */
	public function saveAdditionalFields(array $submittedData, tx_scheduler_Task $task) {
		$task->email = $submittedData['tx_cleaner_Delete_email'];
		$task->table = $submittedData['tx_cleaner_Delete_table'];
		$task->pageId = $submittedData['tx_cleaner_Delete_pageId'];
		$task->age = $submittedData['tx_cleaner_Delete_age'];
	}
}

if (defined('TYPO3_MODE') && isset($GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/cleaner/tasks/class.tx_cleaner_delete_additionalfieldprovider.php'])) {
	/** @noinspection PhpIncludeInspection */
	include_once($GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/cleaner/tasks/class.tx_cleaner_delete_additionalfieldprovider.php']);
}

?>
