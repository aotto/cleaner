<?php
/*
 * Register necessary class names with autoloader
 *
 * $Id$
 */
$extensionPath = t3lib_extMgm::extPath('cleaner');
return array(
	'tx_cleaner_markdeleted' => $extensionPath . 'tasks/class.tx_cleaner_markdeleted.php',
	'tx_cleaner_markdeleted_additionalfieldprovider' => $extensionPath . 'tasks/class.tx_cleaner_markdeleted_additionalfieldprovider.php',
	'tx_cleaner_delete' => $extensionPath . 'tasks/class.tx_cleaner_delete.php',
	'tx_cleaner_delete_additionalfieldprovider' => $extensionPath . 'tasks/class.tx_cleaner_delete_additionalfieldprovider.php',
);
?>