<?php
/* $Id$ */

if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

// Get the extensions's configuration
$extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['cleaner']);

// Add cleaner mark deleted task
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['tx_cleaner_MarkDeleted'] = array(
	'extension' => $_EXTKEY,
	'title' => 'LLL:EXT:' . $_EXTKEY . '/locallang.xml:task.markdeleted.name',
	'description' => 'LLL:EXT:' . $_EXTKEY . '/locallang.xml:task.markdeleted.description',
	'additionalFields' => 'tx_cleaner_MarkDeleted_AdditionalFieldProvider'
);

// Add cleaner delete task
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['tx_cleaner_Delete'] = array(
	'extension' => $_EXTKEY,
	'title' => 'LLL:EXT:' . $_EXTKEY . '/locallang.xml:task.delete.name',
	'description' => 'LLL:EXT:' . $_EXTKEY . '/locallang.xml:task.delete.description',
	'additionalFields' => 'tx_cleaner_Delete_AdditionalFieldProvider'
);
?>