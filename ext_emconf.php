<?php

########################################################################
# Extension Manager/Repository config file for ext "cleaner".
#
# Auto generated 04-09-2011 13:31
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Cleaner',
	'description' => 'Cleaner provides a scheduler task to help you clean up your TYPO3 instance.',
	'category' => 'misc',
	'author' => 'Andreas Otto',
	'author_email' => 'andreas@otto-hanika.de',
	'shy' => '',
	'dependencies' => 'scheduler',
	'conflicts' => '',
	'priority' => '',
	'module' => '',
	'doNotLoadInFE' => 1,
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author_company' => '',
	'version' => '1.1.2',
	'constraints' => array(
		'depends' => array(
			'scheduler' => '',
			'typo3' => '4.3.0-0.0.0',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:14:{s:9:"ChangeLog";s:4:"7b92";s:16:"ext_autoload.php";s:4:"e537";s:12:"ext_icon.gif";s:4:"212a";s:17:"ext_localconf.php";s:4:"7e44";s:14:"ext_tables.php";s:4:"eda3";s:13:"locallang.xml";s:4:"4bc6";s:17:"locallang_csh.xml";s:4:"3a4a";s:14:"doc/manual.odt";s:4:"3f3c";s:14:"doc/manual.pdf";s:4:"66e3";s:14:"doc/manual.sxw";s:4:"4879";s:33:"tasks/class.tx_cleaner_delete.php";s:4:"da2c";s:57:"tasks/class.tx_cleaner_delete_additionalfieldprovider.php";s:4:"f129";s:38:"tasks/class.tx_cleaner_markdeleted.php";s:4:"c04b";s:62:"tasks/class.tx_cleaner_markdeleted_additionalfieldprovider.php";s:4:"d572";}',
	'suggests' => array(
	),
);
?>